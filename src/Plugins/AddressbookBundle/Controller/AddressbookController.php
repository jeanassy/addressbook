<?php

namespace Plugins\AddressbookBundle\Controller;

use Plugins\AddressbookBundle\Entity\Addressbook;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Addressbook controller.
 *
 * @Route("/addressbook")
 */
class AddressbookController extends Controller
{
    /**
     * Lists all addressbook entities.
     *
     * @Route("/", name="addressbook_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $addressbooks = $em->getRepository('PluginsAddressbookBundle:Addressbook')->findAll();

        return $this->render('addressbook/index.html.twig', array(
            'addressbooks' => $addressbooks,
        ));
    }

    /**
     * Creates a new addressbook entity.
     *
     * @Route("/new", name="addressbook_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $addressbook = new Addressbook();
        $form = $this->createForm('Plugins\AddressbookBundle\Form\AddressbookType', $addressbook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($addressbook);
            $em->flush();

            return $this->redirectToRoute('addressbook_show', array('id' => $addressbook->getId()));
        }

        return $this->render('addressbook/form.html.twig', array(
            'addressbook' => $addressbook,
            'form' => $form->createView(),
             'delete_form' =>null,
        ));
    }

    /**
     * Finds and displays a addressbook entity.
     *
     * @Route("/{id}", name="addressbook_show")
     * @Method("GET")
     */
    public function showAction(Addressbook $addressbook)
    {
        $deleteForm = $this->createDeleteForm($addressbook);

        return $this->render('addressbook/show.html.twig', array(
            'addressbook' => $addressbook,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing addressbook entity.
     *
     * @Route("/{id}/edit", name="addressbook_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Addressbook $addressbook)
    {
        $deleteForm = $this->createDeleteForm($addressbook);
        $editForm = $this->createForm('Plugins\AddressbookBundle\Form\AddressbookType', $addressbook);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('addressbook_edit', array('id' => $addressbook->getId()));
        }

        return $this->render('addressbook/form.html.twig', array(
            'addressbook' => $addressbook,
            'form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a addressbook entity.
     *
     * @Route("/delete/{id}", name="addressbook_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Addressbook $addressbook)
    {
        $form = $this->createDeleteForm($addressbook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
             
            $em = $this->getDoctrine()->getManager();
            $em->remove($addressbook);
            $em->flush();
        }

        return $this->redirectToRoute('addressbook_index');
    }

    /**
     * Creates a form to delete a addressbook entity.
     *
     * @param Addressbook $addressbook The addressbook entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Addressbook $addressbook)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('addressbook_delete', array('id' => $addressbook->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
