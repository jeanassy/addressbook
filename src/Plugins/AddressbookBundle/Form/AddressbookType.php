<?php

namespace Plugins\AddressbookBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;

class AddressbookType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('firstname')
                ->add('lastname')
                ->add('country', CountryType::class, [])
                ->add('city')
                ->add('street')
                ->add('zipcode')
                ->add('email')
                ->add('phone')
                ->add('birthdate', BirthdayType::class)
                ->add('file', FileType::class, [
                    "label" => " Image",
                    "required" => false,
                    "attr" => [
                        "accept" => "image/*",
                        "class" => "",
                    ]
        ]);
    }

/**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Plugins\AddressbookBundle\Entity\Addressbook'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'addressbook';
    }

}
